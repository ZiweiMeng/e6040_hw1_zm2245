import os
from os import walk
import numpy as np
from numpy import linalg as LA
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from PIL import Image

import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs

'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''

def plot_mul(c, D, im_num, X_mn, num_coeffs):
    '''
    Plots nine PCA reconstructions of a particular image using number
    of components specified by num_coeffs

    Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the images
        n represents the maximum dimension of the PCA space.
        m represents the number of images

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)

    im_num: Integer
        index of the image to visualize

    X_mn: np.ndarray
        a matrix representing the mean image
    '''
    f, axarr = plt.subplots(3, 3)

    for i in range(3):
        for j in range(3):
            nc = num_coeffs[i*3+j]
            cij = c[:nc, im_num]
            Dij = D[:, :nc]
            plot(cij, Dij, X_mn, axarr[i, j])

    f.savefig('output/hw1b_im{0}.png'.format(im_num))
    plt.close(f)


def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of a image

    imname: string
        name of file where image will be saved.
    '''
    try:
        f, axarr = plt.subplots(4, 4)
    
        for i in range(4):
            for j in range(4):
                axarr[i,j].imshow(D[:,4*i+j].reshape((sz,sz)), cmap = cm.Greys_r)

        f.savefig(imname)
        plt.close(f)
    except:
        raise NotImplementedError


def plot(c, D, X_mn, ax):
    '''
    Plots a reconstruction of a particular image using D as the basis matrix and c as
    the coefficient vector
    Parameters
    -------------------
        c: np.ndarray
            a l x 1 vector  representing the coefficients of the image.
            l represents the dimension of the PCA space used for reconstruction

        D: np.ndarray
            an N x l matrix representing first l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in the image)

        X_mn: basis vectors represent the divergence from the mean so this
            matrix should be added to the reconstructed image

        ax: the axis on which the image will be plotted
    '''
    try:
        X_re = X_mn + c.T.dot(D.T).reshape((256,256))
        ax.imshow(X_re, cmap = cm.Greys_r)
    except:
        raise NotImplementedError


if __name__ == '__main__':
    '''
    Read all images(grayscale) from jaffe folder and collapse each image
    to get an numpy array Ims with size (no_images, height*width).
    Make sure to sort the filenames before reading the images
    '''
    mydir = os.getcwd() + '\\jaffe'
    #my directory to store all the images 
    image_list = []
    for (dirpath, dirnames, filenames) in walk(mydir):
        image_list.extend(filenames)
    image_list.sort()
    #read all filenames into a list
    no_images = len(image_list)
    Ims = np.ndarray((no_images, 256*256))
    i = 0 
    for filename in image_list:
        filename = '\\'.join([mydir,filename])
        image = np.array(Image.open(filename))
        Ims[i] = image.reshape((1,256*256))
        i += 1
    
    
    
    I = Ims

    Ims = I.astype(np.float32)
    X_mn = np.mean(Ims, 0)#65536x1
    X = Ims - np.repeat(X_mn.reshape(1, -1), Ims.shape[0], 0)#213x65536

    '''
    Use theano to perform gradient descent to get top 16 PCA components of X
    Put them into a matrix D with decreasing order of eigenvalues

    If you are not using the provided AMI and get an error "Cannot construct a ufunc with more than 32 operands" :
    You need to perform a patch to theano from this pull(https://github.com/Theano/Theano/pull/3532)
    Alternatively you can downgrade numpy to 1.9.3, scipy to 0.15.1, matplotlib to 1.4.2
    '''
    X_inp = T.matrix("X_inp") #X
    Lambda = T.matrix("Lambda")#diagnal matrix lambda0~lambda_{i-1}
    D_inp = T.matrix("D_inp")#contains d0,d1,...,d_{i-1}
    
    #d = theano.shared(np.random.randn(256*256).T, name="d")
    d = T.vector("d")
    
    cost = -T.dot(X_inp.dot(d).T,X_inp.dot(d)) + T.dot(D_inp.T,d).T.dot(Lambda).dot(D_inp.T.dot(d))
    gd = T.grad(cost, d)
    
    grad_d = theano.function(inputs=[X_inp,Lambda,D_inp,d],outputs=gd)
    
    Tmax = 10000#max iterations
    alpha = 0.001#learning rate
    lim = 0.0001#the small error
    D_in = np.zeros((256*256,16)) 
    Lm = np.zeros((16,16))   
    
    #compute i = 0 separately
    d0 = np.random.randn(256*256).T
    d0 = d0/LA.norm(d0)
    d1 = d0 + alpha*X.T.dot(X.dot(d0))
    d1 = d1/LA.norm(d1)
    #
    #
    t = 1
    while ((t<=Tmax) & (LA.norm(d1 - d0)>lim)):
            d0 = d1
            y = d0 + alpha*X.T.dot(X.dot(d0))
            d1 = y/LA.norm(y)
            t += 1
    Lm[0,0] = (X.dot(d1)).T.dot(X.dot(d1))
    D_in[:,0] = d1
    
    for i in range(1,16):
        t = 1
        lm = Lm[:i,:i]
        Din = D_in[:,:i]
        
        d0 = np.random.randn(256*256).T
        d0 = d0/LA.norm(d0)
        
        d1 = d0 - alpha*grad_d(X,lm,Din)
        d1 = d1/LA.norm(d1)
    
        
        while ((t<=Tmax) & (LA.norm(d1 - d0)>lim)):
            d0 = d1
            y = d0 - alpha*grad_d(X,lm,Din)
            d1 = y/LA.norm(y)
            t += 1
        Lm[i,i] = (X.dot(d1)).T.dot(X.dot(d1))
        D_in[:,i] = d1   
            
        D = D_in
        c = D.T.dot(X.T)        
    
    
    
    
    
    
    ##################################
    for i in range(0,200,10):
        plot_mul(c, D, i, X_mn.reshape((256, 256)),
                 [1, 2, 4, 6, 8, 10, 12, 14, 16])

    plot_top_16(D, 256, 'output/hw1b_top16_256.png')